import Ember from 'ember';

export default Ember.Component.extend({
  username: '',
  password: '',

  actions: {
    signup() {
      this.sendAction('signup', this.get('username'), this.get('password'));
    }
  }
});
