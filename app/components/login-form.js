import Ember from 'ember';

export default Ember.Component.extend({
  identification: '',
  password: '',

  actions: {
    login() {
      this.sendAction('login', this.get('identification'), this.get('password'));
    }
  }
});
