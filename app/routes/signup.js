import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    handleSignup(username, password) {
      const url = 'http://localhost:8000/api/users/signup/';

      Ember.$.ajax({
        url: url,
        data: {username, password},
        method: 'POST'
      }).then(res => {
        this.transitionTo('login');
      });
    }
  }
});
