import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    handleLogin(identification, password) {
      console.log(identification, password);
      const credentials = {identification, password};
      const authenticator = 'authenticator:token';

      this.get('session').authenticate(authenticator, credentials);
    }
  }
});
